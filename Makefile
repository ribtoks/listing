.PHONY: build clean deploy

STAGE ?= dev
REGION ?= eu-west-1
GIT_COMMIT ?= $(shell git rev-list -1 HEAD)
SLS ?= npx sls

vendors:
	go mod tidy
	go mod vendor

test:
	go test ./...

build-listing-cli:
	env GOFLAGS="-mod=vendor" go build -o bin/listing-cli cmd/listing-cli/*.go

build-listing-send:
	env GOFLAGS="-mod=vendor" go build -o bin/listing-send cmd/listing-send/*.go

build-listing:
	env GOFLAGS="-mod=vendor" GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -tags lambda.norpc -ldflags="-s -w -X main.GitCommit=$(GIT_COMMIT)" -o bin/listing/bootstrap cmd/listing/*.go

package-listing:
	zip -j bin/listing.zip bin/listing/bootstrap

build-sesnotify:
	env GOFLAGS="-mod=vendor" GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -tags lambda.norpc -ldflags="-s -w -X main.GitCommit=$(GIT_COMMIT)" -o bin/sesnotify/bootstrap cmd/sesnotify/*.go

package-sesnotify:
	zip -j bin/sesnotify.zip bin/sesnotify/bootstrap

build-ladmin:
	env GOFLAGS="-mod=vendor" GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -tags lambda.norpc -ldflags="-s -w -X main.GitCommit=$(GIT_COMMIT)" -o bin/ladmin/bootstrap cmd/ladmin/*.go

package-ladmin:
	zip -j bin/ladmin.zip bin/ladmin/bootstrap

build: build-listing-cli build-listing-send build-listing build-sesnotify build-ladmin

package: package-listing package-sesnotify package-ladmin

clean:
	rm -rf ./bin ./.serverless Gopkg.lock

deploy-domain:
	$(SLS) create_domain --stage '${STAGE}' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --config serverless-api.yml

remove-domain:
	$(SLS) delete_domain --stage '${STAGE}' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --config serverless-api.yml

deploy-db:
	$(SLS) deploy --config serverless-db.yml --stage '$(STAGE)' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --verbose

remove-db:
	$(SLS) remove --config serverless-db.yml --stage '$(STAGE)' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --verbose

deploy-api:
	$(SLS) deploy --config serverless-api.yml --stage '$(STAGE)' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --verbose

info-api:
	$(SLS) info --config serverless-api.yml --stage '$(STAGE)' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --verbose

remove-api:
	$(SLS) remove --config serverless-api.yml --stage '$(STAGE)' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --verbose

deploy-admin:
	$(SLS) deploy --config serverless-admin.yml --stage '$(STAGE)' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --verbose

remove-admin:
	$(SLS) remove --config serverless-admin.yml --stage '$(STAGE)' --region '$(REGION)' --param="gitHash=$(GIT_COMMIT)" --verbose

deploy-all: clean build package deploy-db deploy-api deploy-admin
	@echo "Done for stage=${STAGE} region=${REGION}"

remove-all: remove-api remove-admin remove-db
	@echo "Done for stage=${STAGE} region=${REGION}"
