package main

import (
	"context"
	"encoding/json"
	"log/slog"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"gitlab.com/ribtoks/listing/pkg/common"
	"gitlab.com/ribtoks/listing/pkg/db"
)

var (
	store     common.NotificationsStore
	GitCommit string
)

func handler(ctx context.Context, snsEvent events.SNSEvent) {
	slog.Debug("Processing records", "count", len(snsEvent.Records))

	for _, record := range snsEvent.Records {
		snsRecord := record.SNS
		var sesMessage common.SesMessage
		err := json.Unmarshal([]byte(snsRecord.Message), &sesMessage)
		if err != nil {
			slog.Error("Error parsing message", common.ErrAttr(err))
			continue
		}

		switch sesMessage.NotificationType {
		case "Bounce":
			{
				isTransient := sesMessage.Bounce.BounceType == "Transient"
				for _, r := range sesMessage.Bounce.BouncedRecipients {
					err = store.AddBounce(r.EmailAddress, sesMessage.Mail.Source, isTransient)
					if err != nil {
						slog.Error("Failed to add bounce", common.ErrAttr(err))
					}
				}
			}
		case "Complaint":
			{
				for _, r := range sesMessage.Bounce.BouncedRecipients {
					err = store.AddComplaint(r.EmailAddress, sesMessage.Mail.Source)
					if err != nil {
						slog.Error("Failed to add complaint", common.ErrAttr(err))
					}
				}
			}
		default:
			{
				slog.Error("Unexpected message", "type", sesMessage.NotificationType)
			}
		}
	}
}

func main() {
	common.SetupLogs(false /*verbose*/)

	tableName := os.Getenv("NOTIFICATIONS_TABLE")

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_REGION")),
	})

	if err != nil {
		slog.Error("Failed to create AWS session", common.ErrAttr(err))
		panic(err)
	}

	store = db.NewNotificationsStore(tableName, sess)

	slog.Debug("Starting", "version", GitCommit)
	lambda.Start(handler)
}
