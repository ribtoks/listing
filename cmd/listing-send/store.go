package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log/slog"
	"sync"

	_ "modernc.org/sqlite"
)

type report struct {
	Email      string   `json:"email"`
	Newsletter string   `json:"newsletter"`
	Campaign   string   `json:"campaign"`
	UserID     string   `json:"userID"`
	Timestamp  JSONTime `json:"timestamp"`
	Error      string   `json:"error,omitempty"`
}

type store struct {
	m  sync.Mutex
	db *sql.DB
}

var (
	createStmt = `
	CREATE TABLE IF NOT EXISTS sendlog (
		key TEXT PRIMARY KEY,
		data JSON
	);
	`

	selectSQL = `SELECT data FROM sendlog WHERE key = ?;`

	insertSQL = `INSERT INTO sendlog(key, data) VALUES(?, ?);`
)

func (r *report) Key() string {
	return fmt.Sprintf("%s/%s/%s", r.Newsletter, r.Campaign, r.Email)
}

func NewStore(dsnURI string) (*store, error) {
	db, err := sql.Open("sqlite", dsnURI)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(createStmt)
	if err != nil {
		return nil, err
	}

	if _, err := db.Exec("PRAGMA synchronous = NORMAL;"); err != nil {
		slog.Error("Error executing synchronous pragma", "error", err)
	}

	if _, err := db.Exec("PRAGMA journal_mode = WAL;"); err != nil {
		slog.Error("Error executing journal mode pragma", "error", err)
	}

	if _, err := db.Exec("PRAGMA locking_mode = EXCLUSIVE;"); err != nil {
		slog.Error("Error executing locking mode pragma", "error", err)
	}

	return &store{
		db: db,
	}, nil
}

func (s *store) Close() {
	s.db.Close()
}

func (s *store) Get(key string) (*report, error) {
	s.m.Lock()
	defer s.m.Unlock()

	var jsonData []byte
	row := s.db.QueryRow(selectSQL, key)
	err := row.Scan(&jsonData)
	if err != nil {
		return nil, err
	}

	r := new(report)
	err = json.Unmarshal(jsonData, r)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func (s *store) Add(r *report) error {
	s.m.Lock()
	defer s.m.Unlock()

	jsonData, err := json.Marshal(r)
	if err != nil {
		slog.Error("Failed to generate json for report", "error", err)
		return err
	}

	// Save JSON data to the SQLite database
	_, err = s.db.Exec(insertSQL, r.Key(), string(jsonData))
	if err != nil {
		slog.Error("Failed to save report to DB", "error", err)
		return err
	}

	return nil
}
