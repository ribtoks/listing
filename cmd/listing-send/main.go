package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io"
	"log/slog"
	"net/url"
	"os"
	"path"
	"sync"
	"time"

	"github.com/go-gomail/gomail"
	"gitlab.com/ribtoks/listing/pkg/common"
	"gitlab.com/ribtoks/listing/pkg/email"
)

type stringArrayFlags []string

func (i *stringArrayFlags) String() string {
	return fmt.Sprintf("%v", *i)
}

func (i *stringArrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

var (
	excludeFileFlag         stringArrayFlags
	listFlag                stringArrayFlags
	smtpServerFlag          = flag.String("url", "", "SMTP server url")
	smtpUsernameFlag        = flag.String("user", "", "SMTP username flag")
	smtpPassFlag            = flag.String("pass", "", "SMTP password flag")
	subjectFlag             = flag.String("subject", "", "Email campaign subject")
	fromEmailFlag           = flag.String("from-email", "", "Sender address")
	fromNameFlag            = flag.String("from-name", "", "Sender name")
	htmlTemplateFlag        = flag.String("html-template", "", "Path to html email template")
	txtTemplateFlag         = flag.String("txt-template", "", "Path to text email template")
	unsubscribeTemplateFlag = flag.String("unsubscribe-template", "", "Path to unsubscribe link template")
	configurationSetFlag    = flag.String("configuration-set", "", "AWS configuration set for SES/CloudWatch tracking")
	addSESMessageTagsFlag   = flag.Bool("ses-message-tags", false, "Add message tags to each email for SES/CloudWatch tracking")
	newsletterFlag          = flag.String("newsletter", "", "Newsletter value for SES/CloudWatch tracking (optional)")
	paramsFlag              = flag.String("params", "params.json", "Path to file with common params")
	workersFlag             = flag.Int("workers", 2, "Number of workers to send emails")
	rateFlag                = flag.Int("rate", 25, "Emails per second sending rate")
	dryRunFlag              = flag.Bool("dry-run", false, "Simulate selected action")
	outFlag                 = flag.String("out", "./", "Path to directory for dry run results")
	helpFlag                = flag.Bool("help", false, "Print help")
	logPathFlag             = flag.String("log", "listing-send.log", "Absolute path to log file")
	squareDelimFlag         = flag.Bool("square-delim", false, "Use square delimiters [[,]] instead of curly {{,}}")
	dbFlag                  = flag.String("db", "./send.db", "Path to the database (can also be a DSN or :memory:)")
	campaignFlag            = flag.String("campaign", "", "Campaign ID (also used for SES/CloudWatch tracking)")
	untrustedEmailsFlag     = flag.Bool("untrusted-emails", false, "Double check and autocorrect emails before using")
)

var funcs = template.FuncMap{
	"qescape": url.QueryEscape,
	"safeHTML": func(s string) any {
		return template.HTML(s)
	},
}

const (
	appName           = "listing-send"
	smtpRetryAttempts = 3
	smtpRetrySleep    = 1 * time.Second
)

func main() {
	flag.Var(&listFlag, "list", "Path to json file with email list (can be used multiple times)")
	flag.Var(&excludeFileFlag, "exclude-file", "File with subscribers to ignore (can be used multiple times)")
	flag.Parse()

	if *helpFlag {
		flag.PrintDefaults()
		return
	}

	logfile, err := setupLogging()
	if err != nil {
		defer logfile.Close()
	}

	left, right := "{{", "}}"
	if *squareDelimFlag {
		left, right = "[[", "]]"
	}

	params, err := readParams(*paramsFlag)
	if err != nil {
		panic(err)
	}

	htmlTplPath := params.HTMLTemplate(*htmlTemplateFlag)
	htmlTemplate, err := template.New(path.Base(htmlTplPath)).Funcs(funcs).Delims(left, right).ParseFiles(htmlTplPath)
	if err != nil {
		slog.Error("Failed to parse HTML template", "error", err)
	}

	txtTplPath := params.TextTemplate(*txtTemplateFlag)
	textTemplate, err := template.New(path.Base(txtTplPath)).Funcs(funcs).Delims(left, right).ParseFiles(txtTplPath)
	if err != nil {
		slog.Error("Failed to parse text template", "error", err)
	}

	if htmlTemplate == nil && textTemplate == nil {
		panic("no email template available")
	}

	var unsubscribeTemplate *template.Template
	if len(*unsubscribeTemplateFlag) > 0 {
		unsubscribeTemplate, err = template.New(path.Base(*unsubscribeTemplateFlag)).Funcs(funcs).Delims(left, right).ParseFiles(*unsubscribeTemplateFlag)
		if err != nil {
			panic(err)
		}
	}

	var subscribers []*common.SubscriberEx
	for _, listFile := range listFlag {
		if chunk, cerr := readSubscribers(listFile); cerr != nil {
			panic(err)
		} else {
			subscribers = append(subscribers, chunk...)
		}
	}

	if *untrustedEmailsFlag {
		for _, s := range subscribers {
			s.Email = email.AutoCorrectEmail(s.Email)
		}
	}

	db, err := NewStore(*dbFlag)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	c := &campaign{
		id:                  params.CampaignID(*campaignFlag),
		htmlTemplate:        htmlTemplate,
		textTemplate:        textTemplate,
		unsubscribeTemplate: unsubscribeTemplate,
		params:              params.Params,
		subscribers:         subscribers,
		subject:             params.Subject(*subjectFlag),
		fromEmail:           *fromEmailFlag,
		fromName:            *fromNameFlag,
		rate:                *rateFlag,
		dryRun:              *dryRunFlag,
		configurationSet:    *configurationSetFlag,
		newsletter:          *newsletterFlag,
		messages:            make(chan *message, 10),
		waiter:              &sync.WaitGroup{},
		workersCount:        *workersFlag,
		excludedSubscribers: make(map[string]bool),
		addMessageTags:      *addSESMessageTagsFlag,
		untrustedEmails:     *untrustedEmailsFlag,
	}

	for _, excludeFile := range excludeFileFlag {
		if ss, err := readExcludedSubscribers(excludeFile); err == nil {
			c.addExcludedSubscribers(ss)
		} else {
			slog.Error("Failed to parse excluded subscribers", "file", excludeFile, "error", err)
		}
	}

	c.send(db)
}

// with format=json we save SubscriberEx to the output so we must read them as well
func readExcludedSubscribers(filepath string) ([]*common.SubscriberEx, error) {
	if len(filepath) == 0 {
		return nil, errors.New("exclude filepath is empty")
	}

	f, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	dec := json.NewDecoder(bytes.NewBuffer(data))
	dec.DisallowUnknownFields()

	var subscribers []*common.SubscriberEx
	err = dec.Decode(&subscribers)
	if err != nil {
		return nil, err
	}
	slog.Info("Parsed excluded subscribers", "count", len(subscribers), "file", filepath)

	return subscribers, nil
}

func readSubscribers(filepath string) ([]*common.SubscriberEx, error) {
	f, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	dec := json.NewDecoder(bytes.NewBuffer(data))
	dec.DisallowUnknownFields()

	var subscribers []*common.SubscriberEx
	err = dec.Decode(&subscribers)
	if err != nil {
		return nil, err
	}
	slog.Info("Parsed subscribers", "count", len(subscribers), "file", filepath)

	return subscribers, nil
}

type CampaignParams struct {
	Params map[string]interface{}
}

func (cp *CampaignParams) Subject(value string) string {
	if len(value) > 0 {
		return value
	}

	if item, ok := cp.Params["subject"]; ok {
		if s, ok := item.(string); ok {
			return s
		}
	}

	return ""
}

func (cp *CampaignParams) CampaignID(value string) string {
	if len(value) > 0 {
		return value
	}

	if item, ok := cp.Params["campaignID"]; ok {
		if s, ok := item.(string); ok {
			return s
		}
	}

	return ""
}

func (cp *CampaignParams) templateFile(value, ext string) string {
	if len(value) > 0 {
		slog.Debug("Using default argument for template", "template", value, "extension", ext)
		return value
	}

	if item, ok := cp.Params["type"]; ok {
		if s, ok := item.(string); ok {
			pathsToCheck := []string{fmt.Sprintf("layouts/%s.%s", s, ext), fmt.Sprintf("%s.%s", s, ext), s}
			for _, p := range pathsToCheck {
				if _, err := os.Stat(p); err == nil {
					slog.Debug("Template file found", "path", p, "extension", ext)
					return p
				}
			}
		}
	}

	return ""
}

func (cp *CampaignParams) HTMLTemplate(value string) string {
	return cp.templateFile(value, "html")
}

func (cp *CampaignParams) TextTemplate(value string) string {
	return cp.templateFile(value, "txt")
}

func readParams(filepath string) (*CampaignParams, error) {
	f, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	params := make(map[string]interface{})
	err = json.Unmarshal(data, &params)
	return &CampaignParams{Params: params}, err
}

func setupLogging() (f *os.File, err error) {
	f, err = os.OpenFile(*logPathFlag, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return nil, err
	}

	opts := &slog.HandlerOptions{
		Level: slog.LevelDebug,
	}
	handler := slog.NewJSONHandler(f, opts)
	logger := slog.New(handler)
	slog.SetDefault(logger)

	slog.Debug("------------------------------")
	slog.Debug(appName + " log started")

	return f, err
}

func flagOrEnv(value, key string) string {
	if len(value) > 0 {
		return value
	}

	if v, ok := os.LookupEnv(key); ok {
		return v
	}

	return ""
}

func createSender() (gomail.SendCloser, error) {
	if *dryRunFlag {
		return &dryRunSender{out: *outFlag}, nil
	}

	dialer, err := smtpDialer(
		flagOrEnv(*smtpServerFlag, "SMTP_SERVER"),
		flagOrEnv(*smtpUsernameFlag, "SMTP_USER"),
		flagOrEnv(*smtpPassFlag, "SMTP_PASS"))
	if err != nil {
		return nil, err
	}

	var sender gomail.SendCloser
	for i := 0; i < smtpRetryAttempts; i++ {
		sender, err = dialer.Dial()
		if err == nil {
			slog.Info("Dialed to SMTP", "server", *smtpServerFlag)
			break
		} else {
			slog.Error("Failed to dial SMTP", "error", err, "attempt", i)
			slog.Debug("Sleeping before retry", "interval", smtpRetrySleep)
			time.Sleep(smtpRetrySleep)
		}
	}
	return sender, nil
}
