package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"log/slog"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/go-gomail/gomail"
	"gitlab.com/ribtoks/listing/pkg/common"
	"gitlab.com/ribtoks/listing/pkg/email"
)

type message struct {
	msg        *gomail.Message
	subscriber *common.SubscriberEx
}

type campaign struct {
	id                  string
	htmlTemplate        *template.Template
	textTemplate        *template.Template
	unsubscribeTemplate *template.Template
	params              map[string]interface{}
	subscribers         []*common.SubscriberEx
	subject             string
	fromEmail           string
	fromName            string
	configurationSet    string
	newsletter          string
	rate                int
	workersCount        int
	waiter              *sync.WaitGroup
	messages            chan *message
	excludedSubscribers map[string]bool
	sentCount           int32
	dryRun              bool
	addMessageTags      bool
	untrustedEmails     bool
}

const xMailer = "listing/0.1 (https://gitlab.com/ribtoks/listing)"

func (c *campaign) send(db *store) {
	slog.Debug("Starting to send messages", "from", c.fromEmail, "dry-run", c.dryRun, "subscribers", len(c.subscribers))
	c.waiter.Add(1)
	go c.generateMessages()
	slog.Debug("Starting workers", "count", c.workersCount)
	for i := 0; i < c.workersCount; i++ {
		go c.sendMessages(i, db)
	}
	slog.Debug("Waiting for workers", "count", c.workersCount)
	c.waiter.Wait()
	close(c.messages)
	slog.Debug("Finished sending messages", "count", c.sentCount)
}

func (c *campaign) addExcludedSubscribers(ss []*common.SubscriberEx) {
	if len(ss) == 0 {
		return
	}

	count := len(c.excludedSubscribers)

	for _, s := range ss {
		c.excludedSubscribers[s.Email] = true
	}

	slog.Info("Added excluded subscribers", "count", len(c.excludedSubscribers)-count)
}

func (c *campaign) generateMessages() {
	defer c.waiter.Done()
	rate := time.Second / time.Duration(c.rate)
	throttle := time.Tick(rate)

	for _, s := range c.subscribers {
		if _, ok := c.excludedSubscribers[s.Email]; ok {
			slog.Warn("Skipping excluded subscriber", "email", s.Email)
			continue
		}

		if c.untrustedEmails && !email.CouldBeValidEmail(s.Email) {
			slog.Warn("Skipping suspicious subscriber", "email", s.Email)
			continue
		}

		m := gomail.NewMessage()
		if err := c.renderMessage(m, s); err != nil {
			slog.Error("Failed to render message", "error", err)
			return
		}
		<-throttle // rate limit
		c.waiter.Add(1)
		c.messages <- &message{msg: m, subscriber: s}
	}
}

func (c *campaign) renderMessage(m *gomail.Message, s *common.SubscriberEx) error {
	data, err := json.Marshal(s)
	if err != nil {
		return err
	}
	recepient := make(map[string]interface{})
	err = json.Unmarshal(data, &recepient)
	if err != nil {
		return err
	}
	ctx := make(map[string]interface{})
	ctx["Params"] = c.params
	ctx["Recepient"] = recepient
	ctx["CurrentYear"] = time.Now().Year()

	m.Reset() // Return to NewMessage state

	var htmlBodyTpl bytes.Buffer
	if c.htmlTemplate != nil {
		if err := c.htmlTemplate.Execute(&htmlBodyTpl, ctx); err != nil {
			return err
		}
	}

	var textBodyTpl bytes.Buffer
	if c.textTemplate != nil {
		if err := c.textTemplate.Execute(&textBodyTpl, ctx); err != nil {
			return err
		}
	}

	if c.unsubscribeTemplate != nil {
		var unsubscribeTpl bytes.Buffer
		if err := c.unsubscribeTemplate.Execute(&unsubscribeTpl, ctx); err == nil {
			unsubscribeLink := strings.TrimSpace(unsubscribeTpl.String())
			if len(unsubscribeLink) > 0 {
				m.SetHeader("List-Unsubscribe-Post", "List-Unsubscribe=One-Click")
				m.SetHeader("List-Unsubscribe", unsubscribeLink)
			}
		} else {
			slog.Error("Error when executing unsubscribe template", "error", err)
		}
	}

	m.SetAddressHeader("To", s.Email, s.Name)
	m.SetAddressHeader("From", c.fromEmail, c.fromName)
	m.SetHeader("Subject", c.subject)
	m.SetHeader("X-Mailer", xMailer)
	tags := make([]string, 0, 2)
	if len(c.configurationSet) > 0 {
		// https://docs.aws.amazon.com/ses/latest/dg/event-publishing-send-email.html#event-publishing-using-ses-headers
		m.SetHeader("X-SES-CONFIGURATION-SET", c.configurationSet)
		// NOTE: adding tags/dimensions will _very_ likely incur money
		if c.addMessageTags {
			if len(c.newsletter) > 0 {
				tags = append(tags, fmt.Sprintf("newsletter=%s", c.newsletter))
			}
			if len(c.id) > 0 {
				tags = append(tags, fmt.Sprintf("campaign=%s", c.id))
			}
			if len(tags) > 0 {
				m.SetHeader("X-SES-MESSAGE-TAGS", strings.Join(tags, ", "))
			}
		}
	}
	hasBody := false
	if txtBody := textBodyTpl.String(); len(txtBody) > 0 {
		m.SetBody("text/plain", txtBody)
		hasBody = true
	}
	if htmlBody := htmlBodyTpl.String(); len(htmlBody) > 0 {
		m.AddAlternative("text/html", htmlBody)
		hasBody = true
	}
	if !hasBody {
		return errors.New("no email body was generated")
	}

	slog.Debug("Rendered email message", "to", s.Email, "sesTags", len(tags))
	return nil
}

func (c *campaign) sendMessages(id int, db *store) {
	slog.Debug("Started sending messages worker", "id", id)
	sender, err := createSender()
	if err != nil {
		panic(err)
	}
	for m := range c.messages {
		r := &report{
			Email:      m.subscriber.Email,
			Newsletter: m.subscriber.Newsletter,
			Campaign:   c.id,
			UserID:     m.subscriber.UserID,
		}
		if len(c.newsletter) > 0 {
			r.Newsletter = c.newsletter
		}

		if existing, err := db.Get(r.Key()); err == nil {
			slog.Info("Skipping sending duplicate report", "to", m.subscriber.Email, "timestamp", existing.Timestamp, "key", r.Key())
			c.waiter.Done()
			continue
		}

		if err := gomail.Send(sender, m.msg); err != nil {
			slog.Error("Error sending message", "id", id, "to", m.subscriber.Email, "error", err)
			r.Error = err.Error()
			sender.Close()
			sender, err = createSender()
			if err != nil {
				panic(err)
			}
		} else {
			slog.Info("Sent email", "id", id, "to", m.subscriber.Email)
		}
		atomic.AddInt32(&c.sentCount, 1)
		r.Timestamp = JSONTimeNow()
		if err := db.Add(r); err != nil {
			slog.Error("Failed to add db record", "error", err)
		}
		c.waiter.Done()
	}
	sender.Close()
}
