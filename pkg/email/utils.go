package email

import (
	"log/slog"
	"strings"

	"github.com/badoux/checkmail"
)

const (
	minUsernameLength  = 2
	maxUsernameLength  = 18
	minDomainLength    = 4 // 1.23
	longWordLength     = 15
	midWordLength      = 10
	smallWordLength    = 7
	smallVowelsCount   = 3
	smallerVowelsCount = 2
)

func AutoCorrectEmail(email string) string {
	parts := strings.Split(email, "@")
	switch parts[1] {
	// NOTE: do NOT autocorrect ymail to gmail as apparently it's yahoo mail now
	case "gmail.co", "gamil.com", "gmail.om", "gmaill.com", "gail.com", "gmail. com", "gmial.com", "gmaiil.com", "@gmil.com",
		"@gmail.clm", "@gmal.com", "@gmail.ocm":
		parts[1] = "gmail.com"
	case "hotmai.de", "hotmail. de":
		parts[1] = "hotmail.de"
	case "hotmai.es":
		parts[1] = "hotmail.es"
	case "hotmai.com", "hotmail. com", "hotamil.com":
		parts[1] = "hotmail.com"
	case "icoud.com", "icloud.co", "icloud.om", "icloud. com":
		parts[1] = "icloud.com"
	}
	return strings.Join(parts, "@")
}

func isASDF(s string) bool {
	anyNonASDF := false

	for _, c := range s {
		if c != 'a' && c != 's' && c != 'd' && c != 'f' {
			anyNonASDF = true
			break
		}
	}

	return !anyNonASDF
}

func isABCD(s string) bool {
	anyNonABCD := false

	for _, c := range s {
		if c != 'a' && c != 'b' && c != 'c' && c != 'd' {
			anyNonABCD = true
			break
		}
	}

	return !anyNonABCD
}

func vowelsCount(s string) int {
	count := 0

	for _, c := range s {
		// we disable 'y' and 'j' as they cannot be a "single" vowel
		if c == 'a' || c == 'e' /*|| c == 'y'*/ || c == 'u' || c == 'i' || c == 'o' /*|| c == 'j'*/ {
			count++
		}
	}

	return count
}

func lettersCount(s string) int {
	count := 0

	for _, c := range s {
		if (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') {
			count++
		}
	}

	return count
}

func isShortOnVowels(s string) bool {
	count := vowelsCount(s)
	if count == 0 {
		return true
	}

	if !strings.Contains(s, ".") {
		lettersLen := lettersCount(s)

		if (lettersLen >= longWordLength) && (count <= smallVowelsCount) {
			return true
		}

		if (lettersLen >= midWordLength) && (count <= smallerVowelsCount) {
			return true
		}

		if (lettersLen >= smallWordLength) && (count <= 1) {
			return true
		}
	}

	return false
}

func isMostlyDigits(s string) bool {
	lettersCount := 0
	digitsCount := 0
	for _, c := range s {
		if ('0' <= c) && (c <= '9') {
			digitsCount++
		} else if (('a' <= c) && (c <= 'z')) || (('A' <= c) && (c <= 'Z')) {
			lettersCount++
		}
	}

	if digitsCount > lettersCount {
		return lettersCount <= 3
	}

	return false
}

func isPlausibleTLD(tld string) bool {
	switch tld {
	case "com", "net", "me", "io", "org", "dev", "edu", "gov", "art", "biz", "info", "lol", "xyz":
		return true
	case "de", "fr", "it", "uk", "ca", "ua", "fi", "us", "in", "ch", "eu", "be", "es", "pl", "pt", "nz", "au":
		return true
	case "test":
		return false
	default:
		return len(tld) <= 4 && vowelsCount(tld) > 0
	}
}

func CouldBeValidEmail(email string) bool {
	if len(email) == 0 {
		return false
	}

	before, after, found := strings.Cut(email, "@")
	if !found || (len(before) <= minUsernameLength) || (len(after) < minDomainLength) {
		return false
	}

	if isASDF(before) || isABCD(before) || isShortOnVowels(before) || isMostlyDigits(before) {
		return false
	}

	if len(before) >= maxUsernameLength && !strings.Contains(before, ".") {
		return false
	}

	domain, tld, found := strings.Cut(after, ".")
	if !found {
		return false
	}

	if !isPlausibleTLD(tld) {
		return false
	}

	if isASDF(domain) || isShortOnVowels(domain) || ((domain == "gmail") && (len(before) <= 4)) {
		return false
	}

	err := checkmail.ValidateFormat(email)
	if err != nil {
		slog.Warn("Failed to validate email format", "error", err, "email", email)
		return false
	}

	return true
}
