package email

import (
	"fmt"
	"testing"
)

func TestAutocorrectEmail(t *testing.T) {
	testCases := []struct {
		wrong   string
		correct string
	}{
		{"test@gmail.co", "test@gmail.com"},
		{"test@icoud.com", "test@icloud.com"},
		{"test@hotmai.com", "test@hotmail.com"},
		{"test@hotamil.com", "test@hotmail.com"},
		// no typo
		{"test@test.com", "test@test.com"},
	}

	for i, tc := range testCases {
		t.Run(fmt.Sprintf("autocorrect_%v", i), func(t *testing.T) {
			email := AutoCorrectEmail(tc.wrong)
			if email != tc.correct {
				t.Errorf("Corrected email does not match. email=%v expected=%v", email, tc.correct)
			}
		})
	}
}

func TestCouldBeValidEmail(t *testing.T) {
	testCases := []struct {
		email string
		valid bool
	}{
		{"", false},
		{"fgf@dfds.bvc", false},
		{"abc@gmail.com", false},
		{"bhkixeoudjaipfgcmh@bbitj.com", false},
		{"elpcgijqbemftqobmn@tcwlx.com", false},
		{"aasdasdasd@s.com", false},
		{"pblqasdygcuhkauylr@tcwlm.com", false},
		{"asdasfas@sdfsdf.oi", false},
		{"s@gmail.com", false},
		{"fdfas@gms.com", false},
		{"sfadf@gmail.com", false},
		{"kdsjfhsdhf@gmail.com", false},
		{"yidnkvradfscdpqpzc@tmmwj.net", false},
		{"test@test.com", true},
		{"something@ukr.net", true},
		{"selenium@gmail.io", true},
		{"ronny.n@gmail.io", true},
		{"erik3@clip3.de", true},
		{"plugin-48271@load.test", false},
		// {"asdlkfjhg@hotmail.com", false}, // currently does not fit into shortOnVowels()
		{"zsdfcasddc@aefse.com", false},
		{"ge592570@gmail.com", false},
		{"test@gmail.com", false},
		{"test1@gmail.com", true},
		{"hefmfvb@gmail.com", false},
		{"sangdes2012@gmail.com", true},
	}

	for i, tc := range testCases {
		t.Run(fmt.Sprintf("valid_%v", i), func(t *testing.T) {
			if CouldBeValidEmail(tc.email) != tc.valid {
				t.Errorf("Valid status does not match. email=%v expected=%v", tc.email, tc.valid)
			}
		})
	}
}
