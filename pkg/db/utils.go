package db

import (
	"context"
	"errors"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

var (
	ErrItemNotFound = errors.New("item not found")
	errInvalidInput = errors.New("invalid input")
)

type keyName string

const (
	keyEmail        keyName = "email"
	keyNotification keyName = "notification"
	keyNewsletter   keyName = "newsletter"
)

// this is the same as getItem1(), but uses both primary and hash keys
func getItem2[T any](ctx context.Context, client dynamodbiface.DynamoDBAPI, tableName string, keyName1 keyName, key1 string, keyName2 keyName, key2 string) (*T, error) {
	if (len(key1) == 0) || (len(key2) == 0) {
		return nil, errInvalidInput
	}

	input := &dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			string(keyName1): &dynamodb.AttributeValue{S: &key1},
			string(keyName2): &dynamodb.AttributeValue{S: &key2},
		},
	}

	return getItemImpl[T](ctx, client, input)
}

func getItemImpl[T any](ctx context.Context, client dynamodbiface.DynamoDBAPI, input *dynamodb.GetItemInput) (*T, error) {
	result, err := client.GetItem(input)
	if err != nil {
		return nil, err
	}

	if len(result.Item) == 0 {
		return nil, ErrItemNotFound
	}

	t := new(T)

	err = dynamodbattribute.UnmarshalMap(result.Item, t)
	if err != nil {
		return nil, err
	}

	return t, nil
}
