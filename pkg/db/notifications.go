package db

import (
	"context"
	"log/slog"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"gitlab.com/ribtoks/listing/pkg/common"
)

// NotificationsDynamoDB is an implementation of Store interface
// that is capable of working with AWS DynamoDB
type NotificationsDynamoDB struct {
	TableName string
	Client    dynamodbiface.DynamoDBAPI
}

var _ common.NotificationsStore = (*NotificationsDynamoDB)(nil)

// NewNotificationsStore returns new instance of NotificationsDynamoDB
func NewNotificationsStore(table string, sess *session.Session) *NotificationsDynamoDB {
	return &NotificationsDynamoDB{
		Client:    dynamodb.New(sess),
		TableName: table,
	}
}

func (s *NotificationsDynamoDB) getNotification(email, t string) (*common.SesNotification, error) {
	return getItem2[common.SesNotification](context.TODO(), s.Client, s.TableName, keyEmail, email, keyNotification, t)
}

func (s *NotificationsDynamoDB) incrementBounce(email, t string) error {
	updateVal := struct {
		Amount int `json:":amount"`
		Zero   int `json:":zero"`
	}{
		Amount: 1,
		Zero:   0,
	}
	update, err := dynamodbattribute.MarshalMap(updateVal)
	if err != nil {
		return err
	}

	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: update,
		ExpressionAttributeNames: map[string]*string{
			"#count": aws.String("count"),
		},
		UpdateExpression:    aws.String("SET #count = if_not_exists(#count, :zero) + :amount"),
		ConditionExpression: aws.String("attribute_exists(email)"),
		TableName:           &s.TableName,
		Key: map[string]*dynamodb.AttributeValue{
			"email": &dynamodb.AttributeValue{
				S: &email,
			},
			"notification": &dynamodb.AttributeValue{
				S: &t,
			},
		},
		ReturnValues: aws.String("UPDATED_NEW"),
	}

	_, err = s.Client.UpdateItem(input)
	if err == nil {
		slog.Info("Updated SES notification", "email", email, "type", t)
	} else if aerr, ok := err.(awserr.Error); ok {
		if aerr.Code() == dynamodb.ErrCodeConditionalCheckFailedException {
			// logging type is error as we shouldn't have arrived to this code branch in the first place
			slog.Error("Notification not found", "email", email, "type", t, "error", aerr.Error())
		}
	}

	return err
}

func (s *NotificationsDynamoDB) storeNotification(email, from string, t string) error {
	i, err := dynamodbattribute.MarshalMap(common.SesNotification{
		Email:        email,
		ReceivedAt:   common.JsonTimeNow(),
		Notification: t,
		From:         from,
		Count:        1,
	})

	if err != nil {
		return err
	}

	_, err = s.Client.PutItem(&dynamodb.PutItemInput{
		TableName: &s.TableName,
		Item:      i,
	})

	if err != nil {
		return err
	}

	slog.Info("Stored notification", "email", email, "type", t)
	return nil
}

func (s *NotificationsDynamoDB) AddBounce(email, from string, isTransient bool) error {
	if isTransient {
		if _, err := s.getNotification(email, common.SoftBounceType); err == nil {
			slog.Debug("Existing notification exists", "email", email, "type", common.SoftBounceType)
			if err := s.incrementBounce(email, common.SoftBounceType); err == nil {
				return nil
			}
		}
	}

	bounceType := common.SoftBounceType
	if !isTransient {
		bounceType = common.HardBounceType
	}
	return s.storeNotification(email, from, bounceType)
}

func (s *NotificationsDynamoDB) AddComplaint(email, from string) error {
	return s.storeNotification(email, from, common.ComplaintType)
}

func (s *NotificationsDynamoDB) Notifications() (notifications []*common.SesNotification, err error) {
	query := &dynamodb.ScanInput{
		TableName: &s.TableName,
	}

	err = s.Client.ScanPages(query, func(page *dynamodb.ScanOutput, more bool) bool {
		var items []*common.SesNotification
		err := dynamodbattribute.UnmarshalListOfMaps(page.Items, &items)
		if err != nil {
			// print the error and continue receiving pages
			slog.Error("Could not unmarshal AWS data", common.ErrAttr(err))
			return true
		}

		notifications = append(notifications, items...)
		// continue receiving pages (can be used to limit the number of pages)
		return true
	})

	return
}

type NotificationsMapStore struct {
	items []*common.SesNotification
}

var _ common.NotificationsStore = (*NotificationsMapStore)(nil)

func (s *NotificationsMapStore) AddBounce(email, from string, isTransient bool) error {
	t := common.SoftBounceType
	if !isTransient {
		t = common.HardBounceType
	}
	s.items = append(s.items, &common.SesNotification{
		Email:        email,
		ReceivedAt:   common.JsonTimeNow(),
		Notification: t,
		From:         from,
		Count:        1,
	})
	return nil
}

func (s *NotificationsMapStore) AddComplaint(email, from string) error {
	s.items = append(s.items, &common.SesNotification{
		Email:        email,
		ReceivedAt:   common.JsonTimeNow(),
		Notification: common.ComplaintType,
		From:         from,
		Count:        1,
	})
	return nil
}

func (s *NotificationsMapStore) Notifications() (notifications []*common.SesNotification, err error) {
	return s.items, nil
}

func NewSubscribersMapStore() *SubscribersMapStore {
	return &SubscribersMapStore{
		items: make(map[string]*common.Subscriber),
	}
}

func NewNotificationsMapStore() *NotificationsMapStore {
	return &NotificationsMapStore{
		items: make([]*common.SesNotification, 0),
	}
}
