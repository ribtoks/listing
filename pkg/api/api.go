package api

import (
	"encoding/json"
	"log/slog"
	"net/http"
	"strings"

	"github.com/badoux/checkmail"
	"gitlab.com/ribtoks/listing/pkg/common"
)

type ListingResource interface {
	Setup(router *http.ServeMux)
	AddNewsletters(n []string)
}

// NewsletterResource manages http requests and data storage
// for newsletter subscriptions
type NewsletterResource struct {
	Secret                 string
	SubscribeRedirectURL   string
	UnsubscribeRedirectURL string
	ConfirmRedirectURL     string
	ConfirmURL             string
	Newsletters            map[string]bool
	Subscribers            common.SubscribersStore
	Notifications          common.NotificationsStore
	Mailer                 common.Mailer
	Captcha                common.CaptchaChecker
}

var _ ListingResource = (*NewsletterResource)(nil)

type AdminResource struct {
	APIToken      string
	Newsletters   map[string]bool
	Subscribers   common.SubscribersStore
	Notifications common.NotificationsStore
}

var _ ListingResource = (*AdminResource)(nil)

const (
	// assume there cannot be such a huge http requests for subscription
	kilobyte             = 1024
	megabyte             = 1024 * kilobyte
	maxSubscribeBodySize = megabyte / 2
	maxImportBodySize    = 25 * megabyte
	maxDeleteBodySize    = 5 * megabyte
)

func (ar *AdminResource) Setup(router *http.ServeMux) {
	router.HandleFunc(common.SubscribersEndpoint, ar.auth(ar.serveSubscribers))
	router.HandleFunc(common.ComplaintsEndpoint, ar.auth(ar.complaints))
}

func (nr *NewsletterResource) Setup(router *http.ServeMux) {
	router.HandleFunc(common.SubscribeEndpoint, nr.method("POST", nr.subscribe))
	router.HandleFunc(common.UnsubscribeEndpoint, nr.method("GET", nr.unsubscribe))
	router.HandleFunc(common.ConfirmEndpoint, nr.method("GET", nr.confirm))
}

func (nr *NewsletterResource) AddNewsletters(n []string) {
	for _, i := range n {
		nr.Newsletters[i] = true
	}
}

func (nr *NewsletterResource) method(m string, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != m {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		next.ServeHTTP(w, r)
	}
}

func (nr *NewsletterResource) isValidNewsletter(n string) bool {
	if n == "" {
		return false
	}

	_, ok := nr.Newsletters[n]

	return ok
}

func (nr *NewsletterResource) validate(newsletter, email string) bool {
	err := checkmail.ValidateFormat(email)
	if err != nil {
		slog.Error("Failed to validate email", "value", email, common.ErrAttr(err))
		return false
	}

	if !nr.isValidNewsletter(newsletter) {
		slog.Warn("Invalid newsletter", "value", newsletter)
		return false
	}

	return true
}

func (nr *NewsletterResource) subscribe(w http.ResponseWriter, r *http.Request) {
	r.Body = http.MaxBytesReader(w, r.Body, maxSubscribeBodySize)
	err := r.ParseForm()

	if err != nil {
		slog.Error("Failed to parse form", common.ErrAttr(err))
	}

	newsletter := r.FormValue(common.ParamNewsletter)
	email := r.FormValue(common.ParamEmail)

	if ok := nr.validate(newsletter, email); !ok {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	recaptcha := r.FormValue(common.ParamRecaptcha)
	if err := nr.Captcha.VerifyResponse(recaptcha); err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	if s, err := nr.Subscribers.GetSubscriber(newsletter, email); err == nil {
		slog.Warn("Subscriber already exists", "email", email, "newsletter", newsletter)

		if s.Confirmed() && !s.Unsubscribed() {
			slog.Warn("Email is already confirmed",
				"email", email, "newsletter", newsletter, "confirmed_at", s.ConfirmedAt.Time())
			w.Header().Set("Location", nr.ConfirmRedirectURL)
			http.Redirect(w, r, nr.ConfirmRedirectURL, http.StatusFound)

			return
		}
	}

	// name is optional
	name := strings.TrimSpace(r.FormValue(common.ParamName))
	source := r.FormValue(common.ParamSource)
	subscriber := common.NewSubscriber(newsletter, email, name)
	subscriber.Source = source

	sslog := slog.With("email", email, "newsletter", newsletter, "name")

	err = nr.Subscribers.AddSubscriber(subscriber)
	if err != nil {
		sslog.Error("Failed to add subscription", common.ErrAttr(err))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	sslog.Info("Added subscription")

	_ = nr.Mailer.SendConfirmation(newsletter, email, name, nr.ConfirmURL)

	w.Header().Set("Location", nr.SubscribeRedirectURL)
	http.Redirect(w, r, nr.SubscribeRedirectURL, http.StatusFound)
}

// unsubscribe route.
func (nr *NewsletterResource) unsubscribe(w http.ResponseWriter, r *http.Request) {
	newsletter := r.URL.Query().Get(common.ParamNewsletter)
	unsubscribeToken := r.URL.Query().Get(common.ParamToken)

	if newsletter == "" {
		http.Error(w, "The newsletter query-string parameter is required", http.StatusBadRequest)
		return
	}

	if !nr.isValidNewsletter(newsletter) {
		http.Error(w, "Invalid newsletter param", http.StatusBadRequest)
		return
	}

	email, ok := common.Unsign(nr.Secret, unsubscribeToken)
	if !ok {
		slog.Error("Failed to unsign token", "token", unsubscribeToken)
		http.Error(w, "Invalid unsubscribe token", http.StatusBadRequest)

		return
	}

	err := nr.Subscribers.RemoveSubscriber(newsletter, email)
	if err != nil {
		slog.Error("Failed to unsubscribe", "email", email, common.ErrAttr(err))
		http.Error(w, "Error unsubscribing from newsletter", http.StatusInternalServerError)

		return
	}

	slog.Info("Unsubscribed", "email", email, "newsletter", newsletter)
	w.Header().Set("Location", nr.UnsubscribeRedirectURL)
	http.Redirect(w, r, nr.UnsubscribeRedirectURL, http.StatusFound)
}

func (nr *NewsletterResource) confirm(w http.ResponseWriter, r *http.Request) {
	newsletter := r.URL.Query().Get(common.ParamNewsletter)
	subscribeToken := r.URL.Query().Get(common.ParamToken)

	if !nr.isValidNewsletter(newsletter) {
		http.Error(w, "Invalid newsletter param", http.StatusBadRequest)
		return
	}

	email, ok := common.Unsign(nr.Secret, subscribeToken)
	if !ok {
		slog.Error("Failed to unsign token", "token", subscribeToken)
		http.Error(w, "Invalid subscribe token", http.StatusBadRequest)

		return
	}

	sslog := slog.With("email", email, "newsletter", newsletter)

	if s, err := nr.Subscribers.GetSubscriber(newsletter, email); err == nil {
		if s.Unsubscribed() {
			sslog.Warn("Subscriber has already unsubscribed")
			w.Header().Set("Location", nr.UnsubscribeRedirectURL)
			http.Redirect(w, r, nr.UnsubscribeRedirectURL, http.StatusFound)

			return
		}
	} else {
		sslog.Error("Subscriber cannot be found", common.ErrAttr(err))
		http.Error(w, "Error confirming subscription", http.StatusInternalServerError)

		return
	}

	err := nr.Subscribers.ConfirmSubscriber(newsletter, email)
	if err != nil {
		sslog.Error("Failed to confirm subscription", common.ErrAttr(err))
		http.Error(w, "Error confirming subscription", http.StatusInternalServerError)

		return
	}

	sslog.Info("Confirmed subscription")
	w.Header().Set("Location", nr.ConfirmRedirectURL)
	http.Redirect(w, r, nr.ConfirmRedirectURL, http.StatusFound)
}

func (ar *AdminResource) complaints(w http.ResponseWriter, r *http.Request) {
	notifications, err := ar.Notifications.Notifications()
	if err != nil {
		slog.Error("Failed to fetch notifications", common.ErrAttr(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	err = json.NewEncoder(w).Encode(notifications)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// auth middleware.
func (ar *AdminResource) auth(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, pass, ok := r.BasicAuth()
		if !ok {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		if pass != ar.APIToken {
			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	}
}

func (ar *AdminResource) serveSubscribers(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		{
			ar.getSubscribers(w, r)
		}
	case "PUT":
		{
			ar.putSubscribers(w, r)
		}
	case "DELETE":
		{
			ar.deleteSubscribers(w, r)
		}
	default:
		{
			slog.Error("Unsupported method for subscribers", "method", r.Method)
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
	}
}

func (ar *AdminResource) getSubscribers(w http.ResponseWriter, r *http.Request) {
	newsletter := r.URL.Query().Get(common.ParamNewsletter)
	nlog := slog.With("newsletter", newsletter)
	nlog.Debug("Serving subscribers")

	if !ar.isValidNewsletter(newsletter) {
		nlog.Error("Invalid newsletter")
		http.Error(w, "The newsletter parameter is invalid", http.StatusBadRequest)
		return
	}

	emails, err := ar.Subscribers.Subscribers(newsletter)
	if err != nil {
		nlog.Error("Failed to fetch subscribers", common.ErrAttr(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	nlog.Info("Fetched subscribers", "count", len(emails))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	err = json.NewEncoder(w).Encode(emails)
	if err != nil {
		nlog.Error("Failed to serialize subscribers to json", common.ErrAttr(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (ar *AdminResource) putSubscribers(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Content-Type header is not application/json", http.StatusUnsupportedMediaType)
		return
	}

	r.Body = http.MaxBytesReader(w, r.Body, maxImportBodySize)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	var subscribers []*common.Subscriber

	err := dec.Decode(&subscribers)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	ss := make([]*common.Subscriber, 0, len(subscribers))

	for _, s := range subscribers {
		if !ar.isValidNewsletter(s.Newsletter) {
			slog.Warn("Skipping unsupported newsletter", "newsletter", s.Newsletter)
			continue
		}

		if err = checkmail.ValidateFormat(s.Email); err != nil {
			slog.Warn("Skipping invalid email", "email", s.Email)
			continue
		}

		if !s.Confirmed() && !s.Unsubscribed() {
			s.CreatedAt = common.JsonTimeNow()
		}

		ss = append(ss, s)
	}

	if len(ss) == 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	err = ar.Subscribers.AddSubscribers(ss)
	if err != nil {
		slog.Error("Failed to import subscribers", common.ErrAttr(err))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusOK)
}

func (ar *AdminResource) deleteSubscribers(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Content-Type header is not application/json", http.StatusUnsupportedMediaType)
		return
	}

	r.Body = http.MaxBytesReader(w, r.Body, maxDeleteBodySize)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	var keys []*common.SubscriberKey

	err := dec.Decode(&keys)
	if err != nil {
		slog.Error("Failed to decode keys", common.ErrAttr(err))
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	err = ar.Subscribers.DeleteSubscribers(keys)
	if err != nil {
		slog.Error("Failed to delete subscribers", common.ErrAttr(err))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusOK)
}

func (ar *AdminResource) isValidNewsletter(n string) bool {
	if n == "" {
		return false
	}

	_, ok := ar.Newsletters[n]

	return ok
}

func (ar *AdminResource) AddNewsletters(n []string) {
	for _, i := range n {
		ar.Newsletters[i] = true
	}
}
