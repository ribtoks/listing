One deployment of Listing is designed to serve a single website/service since confirm/subscribe/unsubscribe URLs are shared. One deployment does support few "sub-newsletter" subscriber lists (names configured via `supportedNewsletters`) but ultimately they should belong to the same website. It's easy (and cost-effective) to have multiple listing deployments.

Except of quite obvious prerequisites, the typical deployment procedure is as easy as editing `secrets.yml` and running `make deploy-all` in the root of the repository. See detailed steps below.

## Generic prerequisites

*   Buy/Own a custom domain
*   [Create an AWS account](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/)
*   Configure and [verify your domain in AWS SES](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-domain-procedure.html) (Simple Email Service)
*   [Exit "sandbox" mode in SES](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/request-production-access.html) (you will have to contact support)
*   Create "confirm"/"unsubscribed" pages on your website

## Install prerequisites

*   [Go](https://golang.org/dl/)
*   [Node.js](https://nodejs.org/en/download/) (for npm and serverless)
*   Serverless and it's plugins (run `npm install` in repository root)
*   [Configure AWS credentials](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html)

## Edit secrets

Copy example file `secrets.example.yml` to `secrets.<your-deployment>.yml`, where `<your-deployment>` can be `dev` or `prod` or `my-saas-name`. Latter is recommended as you can create a couple of non-conflicting listing deployments.

```bash
cp secrets.example.yml secrets.prod.yml
```

and edit it.

Most of the properties are self-descriptive. Redirect URLs are urls where user will be redirected to after pressing "Confirm", "Subscribe" or "Unsubscribe" buttons. `confirmURL` is an url of one of the lambda functions used for email confirmation (can be arbitrary since it's edited after deployment). `emailFrom` is an email that will be used to send this confirmation email. `supportedNewsletters` is semicolon-separated list of newsletter names. *Listing* will ignore all subscribe/unsubscribe requests for newsletters that are not in this list.

## Configure custom domain

If you want to make _listing_ accessible at a subdomain (e.g. `listing.yourdomain.com`) you will need to do couple of things:

*   Request custom certificate in AWS Certificate manager for region `us-east-1` (even if you plan to deploy to other regions) for your domain
*   Make sure `secrets.yml` from previous step has correct domain(s) configured
*   Run `STAGE=dev REGION=eu-west-1 make deploy-domain` (replace stage and region with your preferences) - this has to be run only once per "lifetime"

You can see those domains in API Gateway section of AWS Console. Domain configuration is in the `domain` property in `secrets.yml` file.

Also you need to request a certificate for the subdomain you are going to use. Check [AWS documentation](https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-custom-domains-prerequisites.html) on how to do it. Note that you need to request the certificate only for `us-east-1` region (custom domains will work with any region).

## Deploy listing

`STAGE=<your-deployment> REGION=eu-west-1 make deploy-all`

This command will compile and deploy whole _listing_ to AWS in the said stage and region. In case you are making changes to API or to Admin parts, you can redeploy them separately from DB. Use commands `make deploy-db`, `make deploy-api` and `make deploy-admin` and their `make remove-xxx` counterparts to deploy and remove separate pieces.

## Configure confirm and redirect URLs

Go to AWS Console UI and in Lambda section find `listing-subscribe` function. Check that `CONFIRM_URL` in it's environmental variables points to a correct URL (in the worst case, it can be the API Gateway address for `listing-confirm` function).

(optional - you can do that in the end) Configure `SUBSCRIBE_REDIRECT_URL`, `UNSUBSCRIBE_REDIRECT_URL`, `CONFIRM_REDIRECT_URL` in the appropriate lambda function to point to the pages on your website.

## Configure SNS topic for bounces and complaints

In order to exit "sandbox" mode in AWS SES you need to have a procedure for handling bounces and complaints. *Listing* provides this functionality, but you have to do 1 manual action.

Go to [AWS Console UI and set Bounce and Complaint](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/configure-sns-notifications.html) SNS topic's ARN for your SES domain to the `listing-ses-notifications` topic. You can find it in `SES -> Domains -> (select your domain) -> Notifications`. Arn will be an output of `serverless deploy` command for `serverless-db.yml` config. Example of such ARN: `arn:aws:sns:eu-west-1:1234567890:dev-listing-ses-notifications`.

## Configure redirect URLs to your website

Open lambda function properties in the AWS management console and set all `_REDIRECT_URL` variables to the appropriate values that point to your website. You will need to have pages for email confirmation, unsubscribe confirmation etc.

