`listing-send` is a simple application that allows to send Go-templated emails to subscribers through SMTP server (AWS SES).

`listing-send` requires path to html and text templates. These templates are executed using common parameters (`-params` option, available in template as `{{.Params.xxx}}`) and recepient params from the subscription list (`-list` option, available in template as `{{.Recepient.xxx}}`).

After execution, rendered emails can be saved locally using `-dry-run` option (they are saved to directory from parameter `-out`) or sent to SMTP server.

## Options

```
> ./listing-send -help
  -configuration-set string
    	AWS configuration set
  -dry-run
    	Simulate selected action
  -exclude-file string
    	File with subscribers to ignore
  -from-email string
    	Sender address
  -from-name string
    	Sender name
  -help
    	Print help
  -html-template string
    	Path to html email template
  -l string
    	Absolute path to log file (default "listing-send.log")
  -list string
    	Path to file with email list (default "list.json")
  -newsletter string
    	Newsletter for SES tracking
  -out string
    	Path to directory for dry run results (default "./")
  -params string
    	Path to file with common params (default "params.json")
  -pass string
    	SMTP password flag
  -rate int
    	Emails per second sending rate (default 25)
  -square-delim
    	Use share delimiters [[,]] instead of curly {{,}}
  -stdout
    	Log to stdout and to logfile
  -subject string
    	Html campaign subject
  -txt-template string
    	Path to text email template
  -unsubscribe-template string
    	Path to unsubscribe link template
  -url string
    	SMTP server url
  -user string
    	SMTP username flag
  -workers int
    	Number of workers to send emails (default 2)
```

## One-click unsubscribe

If you want to have `List-Unsubscribe` header included (as [required](https://aws.amazon.com/blogs/messaging-and-targeting/an-overview-of-bulk-sender-changes-at-yahoo-gmail/) by Gmail and Yahoo), create unsubscribe link template and pass it using `-unsubscribe-template` command-line argument.

## Email tracking

AWS SES provides [email tracking](https://docs.aws.amazon.com/ses/latest/dg/faqs-metrics.html) which is enabled via ["ConfigurationSet"](https://docs.aws.amazon.com/ses/latest/dg/using-configuration-sets.html). You can specify them in `listing-send` via `-configuration-set` and `-newsletter` arguments. Newsletter argument is used as a SES tag.

## Example

```
listing-send -url "smtp://email-smtp.us-east-1.amazonaws.com:587" -user "" -pass "" -subject "Newsletter digest January 2020" -from-email "email@example.com" -from-name "Email from Example" -html-template layouts/layout-1-2-3.html -txt-template layouts/_default.text -params content/newsletters/jan-2020-params.json -list lists/test.json
```
