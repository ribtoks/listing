> NOTE: collecting metrics via AWS CloudWatch incurs about `$0.3` per unique combination of every dimension's value in the metric. (e.g. if you have a dimension `campaign` with 12 different values throughout the year, that's 12 different metrics, from the point of view of AWS Billing)

Currently `listing-send` supports setting 2 (optional) [dimensions for metrics](https://docs.aws.amazon.com/ses/latest/dg/event-publishing-add-event-destination-cloudwatch.html#event-publishing-add-event-destination-cloudwatch-dimensions): `campaign` and `newsletter`. You can set them using command-line arguments with the same names.

To enable setting message tags (dimensions) by `listing-send`, you need to explicitly allow it using `--ses-message-tags` command line argument.

## General monitoring information

AWS SES helps you to track different metrics. Check [the docs](https://docs.aws.amazon.com/ses/latest/dg/monitor-sending-activity.html).

## Open and click tracking

Please read [AWS docs](https://docs.aws.amazon.com/ses/latest/dg/configure-custom-open-click-domains.html). Basically you need to add special markers to the emails that you send.
